package com.rockitgaming.common.etc;

/**
 * Created by dangn on 7/24/16.
 */
public enum Keyword {

    ALL, AND, OR
}
