package com.rockitgaming.data.model.entity.game;


public enum GameStatusEnum {
    ACTIVE, SUSPEND
}
