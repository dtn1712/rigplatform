package com.rockitgaming.data.model.repository.account;

import com.rockitgaming.data.model.TableName;
import com.rockitgaming.data.model.entity.account.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long>{

    Account findByUsername(String username);

    Account findByEmail(String email);

    Account findByPhone(String phone);

    @Query(value = "SELECT CASE COUNT(*) WHEN 1 THEN 'true' ELSE 'false' END FROM "
            + " (SELECT attemptLogin.id FROM " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + " attemptLogin INNER JOIN " + TableName.ACCOUNT_TABLE + " account "
            + " ON attemptLogin.accountId = account.id "
            + " WHERE account.username = :username "
            + " AND account.lastSuccessLoginTime = attemptLogin.loginTime "
            + " AND attemptLogin.isLoginSuccess = TRUE "
            + " AND attemptLogin.isSessionLogout = FALSE "
            + " AND attemptLogin.loginIP = :loginIP) subTable", nativeQuery = true)
    boolean isCurrentLogin(@Param("username") String username, @Param("loginIP") String loginIP);


    @Query(value = "SELECT account.* FROM " + TableName.ACCOUNT_TABLE + " account "
            + " INNER JOIN " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + " attemptLogin "
            + " ON attemptLogin.accountId = account.id "
            + " WHERE account.username = :username "
            + " AND account.lastSuccessLoginTime = attemptLogin.loginTime "
            + " AND attemptLogin.isLoginSuccess = TRUE "
            + " AND attemptLogin.isSessionLogout = FALSE "
            + " AND attemptLogin.loginIP = :loginIP", nativeQuery = true)
    Account getCurrentLoginAccount(@Param("username") String username, @Param("loginIP") String loginIP);

    Long countByUsernameAndEmail(String username, String email);

    @Query(value = "SELECT id FROM " + TableName.ACCOUNT_TABLE + " WHERE username = :username ", nativeQuery = true)
    Long getAccountIdByUsername(@Param("username") String username);

    @Query(value = "SELECT id FROM " + TableName.ACCOUNT_TABLE + " WHERE email = :email ", nativeQuery = true)
    Long getAccountIdByEmail(@Param("email") String email);

    @Query(value = "SELECT id FROM " + TableName.ACCOUNT_TABLE + " WHERE phone = :phone ", nativeQuery = true)
    Long getAccountIdByPhone(@Param("phone") String phone);
}
