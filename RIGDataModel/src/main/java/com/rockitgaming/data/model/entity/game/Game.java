package com.rockitgaming.data.model.entity.game;

import com.rockitgaming.data.model.TableName;
import com.rockitgaming.data.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = TableName.GAME_TABLE)
public class Game extends BaseEntity {

    @Column(name = "key", nullable = false, unique = true)
    private String key;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    private GameTypeEnum gameType;

    @Enumerated(EnumType.STRING)
    private GameStatusEnum gameStatus = GameStatusEnum.ACTIVE;

    @Column(name = "releaseDate")
    private Date releaseDate;

    @Column(name = "note")
    private String note;
}
