package com.rockitgaming.data.model.entity.account;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 1/22/17
 * Time: 5:52 PM
 */
public enum AccountLoginTypeEnum {

    USERNAME, EMAIL, PHONE, TOKEN
}
