package com.rockitgaming.data.model.entity.account;

import com.rockitgaming.data.model.TableName;
import com.rockitgaming.data.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = TableName.ACCOUNT_LOCK_TABLE)
public class AccountLock extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "accountId", nullable = false)
    private Account account;

    @Column(name="lockIp", columnDefinition = "TEXT")
    private String lockIP;

    @Column(name="reason", columnDefinition = "TEXT")
    private String reason;

    @Column(name="duration")
    private int duration;

    @Column(name="lockStartTime")
    private DateTime lockStartTime;

    @Column(name="lockEndTime")
    private DateTime lockEndTime;
}
