package com.rockitgaming.data.model.repository.account;

import com.rockitgaming.data.model.TableName;
import com.rockitgaming.data.model.entity.account.Account;
import com.rockitgaming.data.model.entity.account.AccountAttemptLogin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountAttemptLoginRepository extends JpaRepository<AccountAttemptLogin, Long> {

    @Query(value = " SELECT COUNT(*) FROM " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE +
            " LEFT JOIN " + TableName.ACCOUNT_TABLE + " ON " + TableName.ACCOUNT_TABLE + ".username = :username " +
            " WHERE " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + ".loginTime >= :startTime " +
            " AND " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + ".loginTime <= :endTime " +
            " AND " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + ".loginIP = :loginIP " +
            " AND " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + ".isLoginSuccess = false ", nativeQuery = true)
    int countFailedAttemptByUsernameInPeriod(@Param("username") String username,
                                             @Param("loginIP") String loginIP,
                                             @Param("startTime") String startTime,
                                             @Param("endTime") String endTime);


    @Query(value = " SELECT COUNT(*) FROM " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE +
            " LEFT JOIN " + TableName.ACCOUNT_TABLE + " ON " + TableName.ACCOUNT_TABLE + ".email = :email " +
            " WHERE " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + ".loginTime >= :startTime " +
            " AND " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + ".loginTime <= :endTime " +
            " AND " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + ".loginIP = :loginIP " +
            " AND " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + ".isLoginSuccess = false ", nativeQuery = true)
    int countFailedAttemptByEmailInPeriod(@Param("email") String email,
                                          @Param("loginIP") String loginIP,
                                          @Param("startTime") String startTime,
                                          @Param("endTime") String endTime);


    @Query(value = " SELECT COUNT(*) FROM " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE +
            " LEFT JOIN " + TableName.ACCOUNT_TABLE + " ON " + TableName.ACCOUNT_TABLE + ".phone = :phone " +
            " WHERE " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + ".loginTime >= :startTime " +
            " AND " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + ".loginTime <= :endTime " +
            " AND " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + ".loginIP = :loginIP " +
            " AND " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + ".isLoginSuccess = false ", nativeQuery = true)
    int countFailedAttemptByPhoneInPeriod(@Param("phone") String phone,
                                          @Param("loginIP") String loginIP,
                                          @Param("startTime") String startTime,
                                          @Param("endTime") String endTime);


    @Query(value = "SELECT " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + ".* FROM " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE +
            " LEFT JOIN " + TableName.ACCOUNT_TABLE + " ON " + TableName.ACCOUNT_TABLE + ".username = :username " +
            " ORDER BY " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE + ".loginTime DESC LIMIT 1",
            nativeQuery = true)
    AccountAttemptLogin getLastAttemptLogin(@Param("username") String username);

    List<AccountAttemptLogin> findByAccount(Account account);
}
