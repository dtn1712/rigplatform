package com.rockitgaming.data.model;


public class TableName {

    public static final String ACCOUNT_TABLE = "account";
    public static final String ACCOUNT_LOCK_TABLE = "accountlock";
    public static final String ACCOUNT_ATTEMPT_LOGIN_TABLE = "accountattemptlogin";

    public static final String GAME_TABLE = "game";
    public static final String GAME_SETTINGS_TABLE = "gamesettings";
}
