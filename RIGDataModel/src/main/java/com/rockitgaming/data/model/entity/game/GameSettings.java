package com.rockitgaming.data.model.entity.game;

import com.rockitgaming.data.model.TableName;
import com.rockitgaming.data.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = TableName.GAME_SETTINGS_TABLE)
public class GameSettings extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gameId", nullable = false)
    private Game game;

    @Column(name = "settingsKey")
    private String settingsKey;

    @Column(name = "settingsValue")
    private String settingsValue;
}
