package com.rockitgaming.data.model.entity.account;

public enum AccountTypeEnum {
    NORMAL,
    ADMIN
}
