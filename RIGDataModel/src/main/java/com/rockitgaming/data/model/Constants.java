package com.rockitgaming.data.model;

/**
 * Created by dangn on 8/13/16.
 */
public class Constants {

    public static final int MAX_FAILED_ATTEMPT_LOGIN = 5;
    public static final int MINUTES_DURATION_FOR_MAX_FAILED_ATTEMPT_LOGIN = 15; // in minutes
    public static final int FAILED_LOGIN_ACCOUNT_LOCK_HOUR_DURATION = 1;

    public static final String CACHE_KEY_SEPARATOR = ":::";
}
