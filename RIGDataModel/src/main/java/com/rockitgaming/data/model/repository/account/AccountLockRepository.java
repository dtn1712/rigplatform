package com.rockitgaming.data.model.repository.account;

import com.rockitgaming.data.model.TableName;
import com.rockitgaming.data.model.entity.account.AccountLock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountLockRepository  extends JpaRepository<AccountLock, Long> {

    @Query(value = " SELECT * FROM " + TableName.ACCOUNT_LOCK_TABLE +
                   " LEFT JOIN " + TableName.ACCOUNT_TABLE + " ON " + TableName.ACCOUNT_TABLE + ".username = :username " +
                   " WHERE " + TableName.ACCOUNT_LOCK_TABLE + ".lockStartTime <= :lockStartTime " +
                   " AND " + TableName.ACCOUNT_LOCK_TABLE + ".lockEndTime >= :lockEndTime",
            nativeQuery = true)
    AccountLock getCurrentAccountLockByUsername(@Param("username") String username,
                                                @Param("lockStartTime") String lockStartTime,
                                                @Param("lockEndTime") String lockEndTime);


    @Query(value = " SELECT * FROM " + TableName.ACCOUNT_LOCK_TABLE +
            " LEFT JOIN " + TableName.ACCOUNT_TABLE + " ON " + TableName.ACCOUNT_TABLE + ".email = :email " +
            " WHERE " + TableName.ACCOUNT_LOCK_TABLE + ".lockStartTime <= :lockStartTime " +
            " AND " + TableName.ACCOUNT_LOCK_TABLE + ".lockEndTime >= :lockEndTime",
            nativeQuery = true)
    AccountLock getCurrentAccountLockByEmail(@Param("email") String email,
                                             @Param("lockStartTime") String lockStartTime,
                                             @Param("lockEndTime") String lockEndTime);


    @Query(value = " SELECT * FROM " + TableName.ACCOUNT_LOCK_TABLE +
            " LEFT JOIN " + TableName.ACCOUNT_TABLE + " ON " + TableName.ACCOUNT_TABLE + ".phone = :phone " +
            " WHERE " + TableName.ACCOUNT_LOCK_TABLE + ".lockStartTime <= :lockStartTime " +
            " AND " + TableName.ACCOUNT_LOCK_TABLE + ".lockEndTime >= :lockEndTime",
            nativeQuery = true)
    AccountLock getCurrentAccountLockByPhone(@Param("phone") String phone,
                                             @Param("lockStartTime") String lockStartTime,
                                             @Param("lockEndTime") String lockEndTime);
}
