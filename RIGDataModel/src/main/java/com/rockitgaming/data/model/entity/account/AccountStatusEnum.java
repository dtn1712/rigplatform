package com.rockitgaming.data.model.entity.account;

public enum AccountStatusEnum {
    ACTIVE,
    BANNED,
    INACTIVE,
    LOCKED
}
