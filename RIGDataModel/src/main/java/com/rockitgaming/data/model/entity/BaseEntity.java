package com.rockitgaming.data.model.entity;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@ToString
@MappedSuperclass
public class BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public boolean isNew() {
        return id == null;
    }

    public static final Function<BaseEntity, Long> TO_IDENTITY = input -> input.getId();

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof BaseEntity)) {
            return false;
        }
        BaseEntity entity = (BaseEntity) obj;
        return Objects.equal(this.id, entity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.id);
    }
}
