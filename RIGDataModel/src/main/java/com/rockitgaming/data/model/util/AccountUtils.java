package com.rockitgaming.data.model.util;


import com.rockitgaming.data.model.entity.account.Account;

public class AccountUtils {

    public static Account getSecureAccount(Account rawAccount) {
        if (rawAccount == null) {
            return null;
        }

        Account secureAccount = new Account();
        secureAccount.setId(rawAccount.getId());
        secureAccount.setPhone(rawAccount.getPhone());
        secureAccount.setUsername(rawAccount.getUsername());
        secureAccount.setCreatedAt(rawAccount.getCreatedAt());
        secureAccount.setAccountType(rawAccount.getAccountType());
        secureAccount.setAccountStatus(rawAccount.getAccountStatus());
        secureAccount.setLastSuccessLoginTime(rawAccount.getLastSuccessLoginTime());
        return secureAccount;
    }
}
