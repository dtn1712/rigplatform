package com.rockitgaming.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rockitgaming.common.util.MapperUtils;
import com.rockitgaming.data.model.Constants;
import com.rockitgaming.data.model.entity.BaseEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

public abstract class BaseService {

    private static final Logger logger = LogManager.getLogger(BaseService.class);

    @Autowired
    private CacheService cacheService;

    protected Object getCacheObject(String key, Class clazz) {
        ObjectMapper mapper = MapperUtils.getObjectMapper();
        String data = cacheService.get(key);
        if (data != null) {
            try {
                return mapper.readValue(data,clazz);
            } catch (IOException e) {
                logger.error(String.format("Failed to get object %s of key %s", clazz.getName(), key),e);
            }
        }
        return null;
    }

    protected void setCacheObject(String key, Object object){
        try {
            cacheService.set(key, object);
        } catch (JsonProcessingException e) {
            logger.error(String.format("Failed to set cache object for key %s", key), e);
        }
    }

    protected void deleteCacheObject(String key) {
        cacheService.delete(key);
    }

    protected void deleteBulkCacheObject(String keyPattern) {
        cacheService.deletePattern(keyPattern);
    }

    protected Long setIdFieldValue(Object field) {
        if (field == null) return null;
        BaseEntity baseEntity = (BaseEntity) field;
        return baseEntity.getId();
    }

    protected String generateCacheKey(String className, String methodName) {
        return className + Constants.CACHE_KEY_SEPARATOR + methodName;
    }

    protected String generateCacheKey(String className, String methodName, String additionalKey) {
        return className + Constants.CACHE_KEY_SEPARATOR + methodName + Constants.CACHE_KEY_SEPARATOR + additionalKey;
    }
}
