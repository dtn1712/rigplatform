package com.rockitgaming.service.exceptions;

/**
 * Created by dangn on 7/25/16.
 */
public class InvalidInputException extends RuntimeException  {

    public InvalidInputException(String message) {
        super(message);
    }

    public InvalidInputException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidInputException(Throwable cause) {
        super(cause);
    }
}
