package com.rockitgaming.service.exceptions;

/**
 * Created by dangn on 7/26/16.
 */
public class AccountRegisterException extends RuntimeException {

    public AccountRegisterException(String message) {
        super(message);
    }

    public AccountRegisterException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountRegisterException(Throwable cause) {
        super(cause);
    }
}
