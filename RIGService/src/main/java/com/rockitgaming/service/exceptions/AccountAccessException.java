package com.rockitgaming.service.exceptions;

/**
 * Created by dangn on 7/23/16.
 */
public class AccountAccessException extends RuntimeException {

    public AccountAccessException(String message) {
        super(message);
    }

    public AccountAccessException(String message, Throwable cause) {
        super(message,cause);
    }

    public AccountAccessException(Throwable cause) {
        super(cause);
    }
}
