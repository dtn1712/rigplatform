package com.rockitgaming.service.exceptions;

/**
 * Created by dangn on 7/27/16.
 */
public class UpdateDataException extends RuntimeException{

    public UpdateDataException(String message) {
        super(message);
    }

    public UpdateDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpdateDataException(Throwable cause) {
        super(cause);
    }
}
