package com.rockitgaming.service.exceptions;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 1/7/17
 * Time: 12:58 AM
 */
public class InvalidRequestException  extends RuntimeException  {

    public InvalidRequestException(String message) {
        super(message);
    }

    public InvalidRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidRequestException(Throwable cause) {
        super(cause);
    }
}
