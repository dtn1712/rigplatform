package com.rockitgaming.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.api.sync.RedisCommands;
import com.rockitgaming.common.util.MapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class CacheService {

    private RedisCommands<String, String> redisCommands;

    @Autowired
    public CacheService(@Qualifier("redisConnection") String redisConnection) {
        RedisClient redisClient = RedisClient.create(redisConnection);
        redisCommands = redisClient.connect().sync();
    }

    public void set(String key, String value) {
        redisCommands.set(key,value);
    }

    public void set(String key, Object value) throws JsonProcessingException {
        ObjectMapper mapper = MapperUtils.getObjectMapper();
        String objectData = mapper.writeValueAsString(value);
        redisCommands.set(key,objectData);
    }

    public String get(String key) {
        return redisCommands.get(key);
    }

    public Object get(String key, Class clazz) throws IOException {
        ObjectMapper mapper = MapperUtils.getObjectMapper();
        String redisData = redisCommands.get(key);
        return mapper.readValue(redisData,clazz);
    }

    public Object getOrSet(String key, Object value, Class clazz) throws IOException {
        ObjectMapper mapper = MapperUtils.getObjectMapper();
        String redisData = redisCommands.get(key);
        if (redisData == null) {
            String objectData = mapper.writeValueAsString(value);
            redisCommands.set(key,objectData);
            return value;
        } else {
            return mapper.readValue(redisData,clazz);
        }
    }

    public String getOrSet(String key, String value) {
        String redisData = redisCommands.get(key);
        if (redisData == null) {
            redisCommands.set(key,value);
            return value;
        } else {
            return redisData;
        }
    }

    public void delete(String key) {
        redisCommands.del(key);
    }

    public void deletePattern(String keyPattern) {
        List<String> keys = redisCommands.keys(keyPattern);
        for (String key : keys) {
            delete(key);
        }
    }

    public boolean ping() {
        return redisCommands.isOpen();
    }

    public void clearAll() {
        redisCommands.flushall();
    }

    public void reset() {
        redisCommands.reset();
    }

    public void increment(String key, long amount) {
        redisCommands.incrby(key,amount);
    }

    public void decrement(String key, long amount) {
        redisCommands.decrby(key,amount);
    }

}
