package com.rockitgaming.service;

import com.rockitgaming.data.model.TableName;
import com.rockitgaming.data.model.entity.account.Account;
import com.rockitgaming.service.exceptions.AccountAccessException;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/spring/beanconfig/application-context.xml")
public class AccountServiceTest {

    @Autowired
    private AccountService accountService;

    @Autowired
    private HikariDataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    @Before
    public void setup() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        clearDatabase();
    }

    @After
    public void tearDown() {
//        clearDatabase();
        jdbcTemplate = null;
    }

    @Test
    public void testPass() {
        Assert.assertTrue(true);
    }

    @Test
    public void testHappyPath() throws Exception{
        Account account = new Account();
        account.setEmail("test1@gmail.com");
        account.setUsername("test1");
        account.setPassword("123456");
        accountService.register(account, "127.0.0.1");
//        accountService.login("username","test1", "123456", "127.0.0.1");
    }

//    @Test
    public void testFailedLogin() throws Exception {
        Account account = new Account();
        account.setEmail("test1@gmail.com");
        account.setUsername("test1");
        account.setPassword("123456");
        accountService.register(account, "127.0.0.1");

        for (int i = 0; i < 10; i++) {
            try {
                accountService.login("username","test1", "1234567890", "127.0.0.1");
            } catch (AccountAccessException e) {}
        }

        Assert.assertTrue(accountService.checkAccountLockedByUsername("test1", "127.0.0.1"));
    }

    private void clearDatabase() {
        jdbcTemplate.execute("DELETE FROM " + TableName.ACCOUNT_LOCK_TABLE);
        jdbcTemplate.execute("DELETE FROM " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE);
        jdbcTemplate.execute("DELETE FROM " + TableName.ACCOUNT_TABLE);
    }
}
