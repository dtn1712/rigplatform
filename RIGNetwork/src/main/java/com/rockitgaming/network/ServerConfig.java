package com.rockitgaming.network;

import org.springframework.util.Assert;

import java.util.Properties;

public class ServerConfig {

	public static String serverIp;
	public static int serverPort;

	// Netty
	public static int connectTimeoutMillis;
	public static int soBackLog;
	public static boolean soKeepAlive;
	public static int systemMsgThreadPoolSize;
	public static int msgThreadPoolSize;

	public static int threadCorePoolSize;
	public static int threadMaxPoolSize;
	public static int threadQueueCapacity;

	// config logic game
	public static int delayTimeMillis;
	public static int maxPlayers;

	// for timer schedule
	public static int serverId;
	public static int delayLoginTime;
	public static Properties properties;


	public static void init(Properties prop) {
		Assert.notNull(prop);

		serverIp = prop.getProperty("server.ip", "127.0.0.1");
		serverPort = Integer.parseInt(prop.getProperty("server.port", "8000"));
		soBackLog = Integer.parseInt(prop.getProperty("soBacklog", "1000"));
		connectTimeoutMillis = Integer.parseInt(prop.getProperty("connectTimeoutMillis", "30000"));
		soKeepAlive = Boolean.parseBoolean(prop.getProperty("soKeepAlive", "true"));
		systemMsgThreadPoolSize = Integer.parseInt(prop.getProperty("systemMessageExecutor.threadPoolSize", "1"));
		msgThreadPoolSize = Integer.parseInt(prop.getProperty("messageExecutor.threadPoolSize", "1"));

		threadCorePoolSize = Integer.parseInt(prop.getProperty("threadCorePoolSize","25"));
		threadMaxPoolSize = Integer.parseInt(prop.getProperty("threadMaxPoolSize","50"));
		threadQueueCapacity = Integer.parseInt(prop.getProperty("threadQueueCapacity","100"));

		// game config
		delayTimeMillis = Integer.parseInt(prop.getProperty("delayTimeMillis", "1000"));
		delayLoginTime = Integer.parseInt(prop.getProperty("delayLoginTime", "5"));
		serverId = Integer.parseInt(prop.getProperty("serverId","1"));
		maxPlayers = Integer.parseInt(prop.getProperty("maxPlayers","2000"));

		properties = new Properties();
		properties.putAll(prop);
	}

}
