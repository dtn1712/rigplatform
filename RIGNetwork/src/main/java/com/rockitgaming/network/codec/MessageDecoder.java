package com.rockitgaming.network.codec;

import com.rockitgaming.network.domain.Message;
import com.rockitgaming.network.util.ConverterUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.util.List;

public class MessageDecoder extends MessageToMessageDecoder<ByteBuf> {

//	private static final int HEADER_LENGTH = 7;
	// max 0.5Mb
//	public static final int MAX_BODY_LENGTH = 512000;

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
//		if (in.readableBytes() < HEADER_LENGTH)
//			throw new RuntimeException("Invalid data length");

		byte protocolVersion = in.readByte();// 1byte protocol version
		short commandId = in.readShort();
		int bodyLength = in.readInt();

		Message message = new Message();
		message.setProtocolVersion(protocolVersion);
		message.setCommandId(commandId);
		if (bodyLength <= 0) {
			out.add(message);
			in.clear();
			return;
		}

		// trường hợp message quá lớn
//		if (bodyLength > MAX_BODY_LENGTH) {
//			in.clear();
//			throw new RuntimeException("Body length is too large");
//		}

		byte[] bodyData = getBodyData(in);
		int i = 0;
		while (i < bodyLength) {
			try {
				// get key
				short key = ConverterUtil.convertBytes2Short(new byte[] { bodyData[i], bodyData[i + 1] }).shortValue();
				int valueLength = ConverterUtil.convertBytes2Integer(new byte[] { bodyData[(i + 2)], bodyData[(i + 3)], bodyData[(i + 4)], bodyData[(i + 5)] });

				if (valueLength > bodyLength) {
					throw new RuntimeException("Invalid data, invalid value length:" + valueLength + " for key " + key);
				}

				byte[] value = new byte[valueLength];
				System.arraycopy(bodyData, i + 6, value, 0, valueLength);
				message.putBytes(key, value);
				i += 6 + valueLength;
			} catch (Exception e) {
				in.clear();
				throw new RuntimeException("Invalid data:" + e.getMessage());
			}
		}

		// khi được add vào list sẽ remove
		out.add(message);
		in.clear();
		
	}

	private byte[] getBodyData(ByteBuf byteBuf) {
		byte[] bytes;
		int length = byteBuf.readableBytes();

		if (byteBuf.hasArray()) {
			bytes = byteBuf.array();
		} else {
			bytes = new byte[length];
			byteBuf.getBytes(byteBuf.readerIndex(), bytes);
		}
		return bytes;
	}

}
