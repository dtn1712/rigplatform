package com.rockitgaming.network.codec;

import com.rockitgaming.network.domain.Message;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

public class MessageEncoder extends MessageToByteEncoder<Message> {

	@Override
	protected void encode(ChannelHandlerContext ctx, Message message, ByteBuf out) throws Exception {
		ByteArrayOutputStream bos = null;
		DataOutputStream bodyDOS = null;

		try {
			List<Short> keyList = message.getKeyList();
			byte[] body = null;
			if (keyList != null && keyList.size() > 0) {
				bos = new ByteArrayOutputStream();
				bodyDOS = new DataOutputStream(bos);
				for (int i = 0, size = keyList.size(); i < size; i++) {
					byte[] value = message.getBytesAt(i);
					bodyDOS.writeShort(keyList.get(i).shortValue());
					bodyDOS.writeInt(value.length);
					bodyDOS.write(value);
				}
				bodyDOS.flush();
				body = bos.toByteArray();
			}

			out.writeByte(message.getProtocolVersion());
			out.writeShort(message.getCommandId());
			if (body != null) {
				out.writeInt(body.length);
				out.writeBytes(body);
			} else {
				out.writeInt(0);
			}
		} catch (IOException e) {
			throw new RuntimeException("Invalid messsage");
		} finally {
			try {
				if (bodyDOS != null) {
					bodyDOS.close();
				}
				if (bos != null) {
					bos.close();
				}
			} catch (Exception localException1) {
			}
		}

	}

}
