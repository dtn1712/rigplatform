package com.rockitgaming.network.exception;


public class RequestException extends RuntimeException{

    public RequestException(String message) {
        super(message);
    }
}
