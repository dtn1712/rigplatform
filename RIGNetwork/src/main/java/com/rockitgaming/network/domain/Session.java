package com.rockitgaming.network.domain;

import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

@Getter
@Setter
public class Session {

    private boolean isAuthenticated;
    private String sessionId;
    private String clientIp;
    private String loginUsername;
    private Object user;
    private DateTime loginTime;
}
