package com.rockitgaming.network.domain;

import com.rockitgaming.network.socket.message.MessageContentInterpreter;
import com.rockitgaming.network.socket.message.MessageParameter;
import com.rockitgaming.network.util.ConverterUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
public class Message {

	public static final short DATA_KEY = 9999;

	private static MessageContentInterpreter interpreter;
	private Short commandId;
	private byte protocolVersion;
	private List<MessageParameter> content = new ArrayList<>();
	private Map<Short, List<Integer>> keyIndexMap = new HashMap<>();


	public byte[] getBlob(short key) {
		MessageParameter keyValueInfo = getKeyValueInfo(key);
		if (keyValueInfo != null) {
			return keyValueInfo.getValue();
		}

		return null;
	}


	public byte[][] getBlobs(short key) {
		List<Integer> keyIndexes = keyIndexMap.get(key);
		if (keyIndexes == null || keyIndexes.size() == 0) {
			return null;
		}

		byte[][] results = new byte[keyIndexes.size()][];
		MessageParameter info = null;
		for (int i = 0, length = results.length; i < length; i++) {
			info = content.get(keyIndexes.get(i));
			if (info != null && info.getValue().length >= 1) {
				results[i] = info.getValue();
			}
		}

		return results;
	}


	public Byte getByte(short key) {
		MessageParameter keyValueInfo = getKeyValueInfo(key);
		if (keyValueInfo != null) {
			return ConverterUtil.convertBytes2Byte(keyValueInfo.getValue());
		}

		return null;
	}


	public byte[] getBytes(short key) {
		List<Integer> keyIndexes = keyIndexMap.get(key);
		if (keyIndexes == null || keyIndexes.size() == 0) {
			return null;
		}

		byte[] results = new byte[keyIndexes.size()];
		MessageParameter info = null;
		for (int i = 0, length = results.length; i < length; i++) {
			info = content.get(keyIndexes.get(i));
			if (info != null && info.getValue().length >= 1) {
				results[i] = info.getValue()[0];
			}
		}

		return results;
	}


	public byte[] getBytesAt(int index) {
		if (index < 0 || index >= content.size()) {
			return null;
		}

		MessageParameter keyValueInfo = content.get(index);
		if (keyValueInfo != null) {
			return keyValueInfo.getValue();
		}

		return null;
	}


	public List<MessageParameter> getContent() {
		return content;
	}


	public Double getDouble(short key) {
		MessageParameter keyValueInfo = getKeyValueInfo(key);
		if (keyValueInfo != null) {
			return ConverterUtil.convertBytes2Double(keyValueInfo.getValue());
		}

		return null;
	}


	public double[] getDoubles(short key) {
		List<Integer> keyIndexes = keyIndexMap.get(key);
		if (keyIndexes == null || keyIndexes.size() == 0) {
			return null;
		}

		double[] results = new double[keyIndexes.size()];
		MessageParameter info = null;
		for (int i = 0, length = results.length; i < length; i++) {
			info = content.get(keyIndexes.get(i));
			if (info != null) {
				results[i] = ConverterUtil.convertBytes2Double(info.getValue()).doubleValue();
			}
		}

		return results;
	}


	public Float getFloat(short key) {
		MessageParameter keyValueInfo = getKeyValueInfo(key);
		if (keyValueInfo != null) {
			return ConverterUtil.convertBytes2Float(keyValueInfo.getValue());
		}

		return null;
	}

	public float[] getFloats(short key) {
		List<Integer> keyIndexes = keyIndexMap.get(key);
		if (keyIndexes == null || keyIndexes.size() == 0) {
			return null;
		}

		float[] results = new float[keyIndexes.size()];
		MessageParameter info = null;
		for (int i = 0, length = results.length; i < length; i++) {
			info = content.get(keyIndexes.get(i));
			if (info != null) {
				results[i] = ConverterUtil.convertBytes2Float(info.getValue()).floatValue();
			}
		}

		return results;
	}


	public Integer getInt(short key) {
		MessageParameter keyValueInfo = getKeyValueInfo(key);
		if (keyValueInfo != null) {
			return ConverterUtil.convertBytes2Integer(keyValueInfo.getValue());
		}

		return null;
	}


	public int[] getInts(short key) {
		List<Integer> keyIndexes = keyIndexMap.get(key);
		if (keyIndexes == null || keyIndexes.size() == 0) {
			return null;
		}

		int[] results = new int[keyIndexes.size()];
		MessageParameter info = null;
		for (int i = 0, length = results.length; i < length; i++) {
			info = content.get(keyIndexes.get(i));
			if (info != null) {
				results[i] = ConverterUtil.convertBytes2Integer(info.getValue()).intValue();
			}
		}

		return results;
	}


	public Short getKeyAt(int index) {
		if (index < 0 || index >= content.size()) {
			return null;
		}

		return content.get(index).getKey();
	}


	public List<Short> getKeyList() {
		if (content == null || content.size() == 0) {
			return null;
		}

		int size = content.size();
		List<Short> keys = new ArrayList<Short>(size);
		for (int i = 0; i < size; i++) {
			keys.add(content.get(i).getKey());
		}

		return keys;
	}


	private MessageParameter getKeyValueInfo(short key) {
		List<Integer> keyIndexes = keyIndexMap.get(key);
		if (keyIndexes == null || keyIndexes.size() == 0) {
			return null;
		}

		return content.get(keyIndexes.get(0));
	}


	public Long getLong(short key) {
		MessageParameter keyValueInfo = getKeyValueInfo(key);
		if (keyValueInfo != null) {
			return ConverterUtil.convertBytes2Long(keyValueInfo.getValue());
		}

		return null;
	}


	public long[] getLongs(short key) {
		List<Integer> keyIndexes = keyIndexMap.get(key);
		if (keyIndexes == null || keyIndexes.size() == 0) {
			return null;
		}

		long[] results = new long[keyIndexes.size()];
		MessageParameter info = null;
		for (int i = 0, length = results.length; i < length; i++) {
			info = content.get(keyIndexes.get(i));
			if (info != null) {
				results[i] = ConverterUtil.convertBytes2Long(info.getValue()).longValue();
			}
		}

		return results;
	}


	public Short getShort(short key) {
		MessageParameter keyValueInfo = getKeyValueInfo(key);
		if (keyValueInfo != null) {
			return ConverterUtil.convertBytes2Short(keyValueInfo.getValue());
		}

		return null;
	}


	public short[] getShorts(short key) {
		List<Integer> keyIndexes = keyIndexMap.get(key);
		if (keyIndexes == null || keyIndexes.size() == 0) {
			return null;
		}

		short[] results = new short[keyIndexes.size()];
		MessageParameter info = null;
		for (int i = 0, length = results.length; i < length; i++) {
			info = content.get(keyIndexes.get(i));
			if (info != null) {
				results[i] = ConverterUtil.convertBytes2Short(info.getValue()).shortValue();
			}
		}

		return results;
	}


	public String getString(short key) {
		MessageParameter keyValueInfo = getKeyValueInfo(key);
		if (keyValueInfo != null) {
			return ConverterUtil.convertBytes2String(keyValueInfo.getValue());
		}

		return null;
	}


	public String[] getStrings(short key) {
		List<Integer> keyIndexes = keyIndexMap.get(key);
		if (keyIndexes == null || keyIndexes.size() == 0) {
			return null;
		}

		String[] results = new String[keyIndexes.size()];
		MessageParameter info = null;
		for (int i = 0, length = results.length; i < length; i++) {
			info = content.get(keyIndexes.get(i));
			if (info != null) {
				results[i] = ConverterUtil.convertBytes2String(info.getValue());
			}
		}

		return results;
	}


	public final void putBytes(short key, byte[] value) {
		MessageParameter info = new MessageParameter();
		info.setKey(key);
		info.setValue(value);
		putKeyValueInfo(key, info);
	}


	public final void putByte(short key, byte value) {
		MessageParameter info = new MessageParameter();
		info.setKey(key);
		info.setValue(new byte[] { value });
		putKeyValueInfo(key, info);
	}


	public final void putDouble(short key, double value) {
		MessageParameter info = new MessageParameter();
		info.setKey(key);
		info.setValue(ConverterUtil.convertDouble2Bytes(Double.valueOf(value)));
		putKeyValueInfo(key, info);
	}


	public final void putFloat(short key, float value) {
		MessageParameter info = new MessageParameter();
		info.setKey(key);
		info.setValue(ConverterUtil.convertFloat2Bytes(Float.valueOf(value)));
		putKeyValueInfo(key, info);
	}


	public final void putInt(short key, int value) {
		MessageParameter info = new MessageParameter();
		info.setKey(key);
		info.setValue(ConverterUtil.convertInteger2Bytes(Integer.valueOf(value)));
		putKeyValueInfo(key, info);
	}


	private void putKeyValueInfo(short key, MessageParameter info) {
		if (info == null || info.getKey() == null || info.getValue() == null) {
			return;
		}

		content.add(info);
		int keyIndex = content.size() - 1;
		List<Integer> keyIndexes = keyIndexMap.get(key);
		if (keyIndexes == null || keyIndexes.size() == 0) {
			keyIndexes = new ArrayList<Integer>();
			keyIndexMap.put(key, keyIndexes);
		}

		keyIndexes.add(Integer.valueOf(keyIndex));
	}


	public final void putLong(short key, long value) {
		MessageParameter info = new MessageParameter();
		info.setKey(key);
		info.setValue(ConverterUtil.convertLong2Bytes(Long.valueOf(value)));
		putKeyValueInfo(key, info);
	}


	public final void putShort(short key, short value) {
		MessageParameter info = new MessageParameter();
		info.setKey(key);
		info.setValue(ConverterUtil.convertShort2Bytes(Short.valueOf(value)));
		putKeyValueInfo(key, info);
	}


	public final void putString(short key, String value) {
		MessageParameter info = new MessageParameter();
		info.setKey(key);
		info.setValue(ConverterUtil.convertString2Bytes(value));
		putKeyValueInfo(key, info);
	}


	@Override
	public String toString() {
		if (interpreter == null) {
			return trace();
		}

		StringBuffer string = new StringBuffer("\n[" + interpreter.interpretCommand(commandId) + "] \n");
		for (MessageParameter info : content) {
			string.append(" " + interpreter.interpretKey(info.getKey()) + "[" + info.getKey() + "] =");
			string.append(" " + interpreter.interpretValue(info.getKey(), info.getValue()) + "\n");
		}
		return string.toString();
	}


	private String trace() {
		StringBuilder sb = new StringBuilder();
		sb.append(" Command Event:" + commandId + ",\n");
		if (content.size() > 0) {
			for (MessageParameter messageParameter : content) {
				sb.append(" " + String.valueOf(messageParameter.getKey()) + ":" + Arrays.toString(messageParameter.getValue()) + ",\n");
			}
			sb.deleteCharAt(sb.length() - 2);
		}

		return "\n{ \n" + sb.toString() + "}";
	}

}
