/**
 * 
 */
package com.rockitgaming.network.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class FileUtils {

    private static final Logger logger = LogManager.getLogger(FileUtils.class);

	/**
	 * Load 1 file text
	 * @param path
	 * @param encoding
	 * @return
	 * @throws IOException
	 */
	public static String loadTextFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return encoding.decode(ByteBuffer.wrap(encoded)).toString();
    }
	
	/**
	 * load file config java
	 * @param path
	 */
	public static Properties loadConfigFile(String path) {
        FileInputStream inputStream;
        Properties returnProperties = null;
        try {
            returnProperties = new Properties();
            inputStream = new FileInputStream(path);
            returnProperties.load(inputStream);
            inputStream.close();
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
        }
        return returnProperties;
    }

}
