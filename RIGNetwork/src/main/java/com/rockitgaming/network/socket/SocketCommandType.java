package com.rockitgaming.network.socket;

/**
 * Socket Command Type is the common command type, it will start with 1000
 */
public class SocketCommandType {

    public static final short COMMAND_ERROR = -1;
    public static final short COMMAND_SUCCESS = 1;

    public static final short COMMAND_CONNECT = 1000;
    public static final short COMMAND_DISCONNECT = 1001;
}
