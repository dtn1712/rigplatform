package com.rockitgaming.network.socket;

/**
 * Socket Error Code is the common error code, it will start with 2000
 */
public class SocketErrorCode {

    public static final short ERROR_UNAUTHORIZED_SESSION = 2000;
    public static final short ERROR_NO_INPUT= 2001;
    public static final short ERROR_INVALID_INPUT = 2002;
    public static final short ERROR_INVALID_INPUT_FORMAT = 2003;
    public static final short ERROR_HANDLE_REQUEST = 2004;
}
