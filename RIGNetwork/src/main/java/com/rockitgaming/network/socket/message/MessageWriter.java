package com.rockitgaming.network.socket.message;

import com.rockitgaming.network.domain.Message;
import com.rockitgaming.network.domain.Session;
import com.rockitgaming.network.socket.SocketServerHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MessageWriter {

	@Autowired
	private SocketServerHandler socketServerHandler;

	public void writeMessage(Session session, Message message) {
		socketServerHandler.send(session, message);
	}

	public void writeMessage(List<Session> sessions, Message message) {
		socketServerHandler.send(sessions, message);
	}


}
