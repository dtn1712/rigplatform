package com.rockitgaming.network.socket.handler;

import com.rockitgaming.network.domain.Message;
import com.rockitgaming.network.domain.Session;
import com.rockitgaming.network.socket.message.MessageWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class BaseRequestHandler implements RequestHandler {

	@Autowired
	private MessageWriter messageWriter;

	protected void writeMessage(Session session, Message message) {
		messageWriter.writeMessage(session, message);
	}

	protected void writeMessage(List<Session> sessions, Message message) {
		messageWriter.writeMessage(sessions, message);
	}

	protected List<String> checkMissingFields(List<String> requiredFields, Map<String, Object> messageData) {
		List<String> missingFields = requiredFields.stream().filter(data -> !messageData.containsKey(data)).collect(Collectors.toList());
		return missingFields;
	}
}
