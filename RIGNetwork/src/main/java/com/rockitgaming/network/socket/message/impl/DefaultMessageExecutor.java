package com.rockitgaming.network.socket.message.impl;

import com.rockitgaming.network.domain.Message;
import com.rockitgaming.network.domain.Session;
import com.rockitgaming.network.exception.RequestException;
import com.rockitgaming.network.socket.handler.RequestHandler;
import com.rockitgaming.network.socket.message.MessageExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

@Component
public class DefaultMessageExecutor implements MessageExecutor {

    private static final Logger logger = LogManager.getLogger(DefaultMessageExecutor.class);

    private static final String MESSAGE_EXECUTOR_THREAD_PREFIX = "message-executor";
    private int counter = 0;

    @Resource(name = "taskExecutor")
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Resource(name = "requestHandlerMap")
    private Map<Short, RequestHandler> requestHandlerMap;

    @Override
    public void execute(Session session,Message message) {
        threadPoolTaskExecutor.execute(new Thread(new MessageExecutorRunnable(session, message), getThreadName(message)));
    }

    private String getThreadName(Message message) {
        return MESSAGE_EXECUTOR_THREAD_PREFIX + "-" + message.getCommandId().toString() + "-" + counter++;

    }

    private class MessageExecutorRunnable implements Runnable {

        private Message message;
        private Session session;

        public MessageExecutorRunnable(Session session, Message message) {
            this.session = session;
            this.message = message;
        }

        @Override
        public void run() {
            if (message != null) {
                try {
                    RequestHandler requestHandler = requestHandlerMap.get(message.getCommandId());
                    if (requestHandler != null) {
                        requestHandler.handle(session, message);
                    } else {
                        throw new RequestException("Cannot find the request handler with this command id");
                    }
                } catch (RequestException e) {
                    logger.error(String.format("Failed to handle request of message command %d for session %s IP %s", message.getCommandId(), session.getSessionId(), session.getClientIp()), e);
                }
            }
        }
    }
}
