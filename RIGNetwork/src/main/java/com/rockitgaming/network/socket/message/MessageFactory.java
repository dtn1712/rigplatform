package com.rockitgaming.network.socket.message;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rockitgaming.common.util.MapperUtils;
import com.rockitgaming.network.domain.Message;
import com.rockitgaming.network.socket.SocketCommandType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class MessageFactory {

	private static final Logger logger = LogManager.getLogger(MessageFactory.class);

	private static final byte PROTOCOL_VERSION = 1;

	public static Message createMessage(short commandId) {
		Message message = new Message();
		message.setCommandId(commandId);
		message.setProtocolVersion(PROTOCOL_VERSION);
		return message;
	}

	public static Message createErrorMessage(short code, String errorMessage) {
		Message message = new Message();
		message.setCommandId(SocketCommandType.COMMAND_ERROR);
		message.setProtocolVersion(PROTOCOL_VERSION);

		Map<String, Object> messageData = new HashMap<>();
		messageData.put("errorCode", code);
		messageData.put("errorMessage", errorMessage);
		try {
			message.putString(Message.DATA_KEY, MapperUtils.getObjectMapper().writeValueAsString(messageData));
		} catch (JsonProcessingException e) {
			logger.error("Failed to convert message data to json string", e);
		}
		return message;
	}


	public static Message createConnectMessage() {
		Message message = new Message();
		message.setCommandId(SocketCommandType.COMMAND_CONNECT);
		message.setProtocolVersion(PROTOCOL_VERSION);
		return message;
	}


	public static Message createDisconnectMessage() {
		Message message = new Message();
		message.setCommandId(SocketCommandType.COMMAND_DISCONNECT);
		message.setProtocolVersion(PROTOCOL_VERSION);
		return message;
	}


}
