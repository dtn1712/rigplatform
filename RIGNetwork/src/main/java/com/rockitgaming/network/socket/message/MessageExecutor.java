package com.rockitgaming.network.socket.message;

import com.rockitgaming.network.domain.Message;
import com.rockitgaming.network.domain.Session;

public interface MessageExecutor {

    void execute(Session session, Message message);
}
