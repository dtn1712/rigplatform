package com.rockitgaming.network.socket.handler;


import com.rockitgaming.network.domain.Message;
import com.rockitgaming.network.domain.Session;


public interface RequestHandler {

	void handle(Session session, Message message);
}
