package com.rockitgaming.network.socket.message;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageParameter {

	private Short key;
	private byte[] value;
}
