package com.rockitgaming.network.socket;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.rockitgaming.network.domain.Message;
import com.rockitgaming.network.domain.Session;
import com.rockitgaming.network.socket.channel.ChannelService;
import com.rockitgaming.network.socket.message.MessageExecutor;
import io.netty.channel.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.SocketAddress;
import java.util.List;

import static com.codahale.metrics.MetricRegistry.name;


public abstract class SocketServerHandler extends SimpleChannelInboundHandler<Message> {

	private static final Logger logger = LogManager.getLogger(SocketServerHandler.class);

	@Autowired
	protected MessageExecutor messageExecutor;

	@Autowired
	protected ChannelService channelService;

	@Autowired
	protected MetricRegistry metrics;

	@Override
	public void channelActive(final ChannelHandlerContext ctx) throws Exception {
		Channel channel = ctx.channel();
		SocketAddress remoteAddress = channel.remoteAddress();

		Session session = channelService.connect(channel);

		handleConnectedRequest(session);

		logger.info("[INFO] [CLIENT] - " + remoteAddress.toString() + " connected -" + "sessionId:" + session.getSessionId());
	}


	@Override
	protected void channelRead0(final ChannelHandlerContext ctx, final Message message) throws Exception {
		Channel channel = ctx.channel();
		Session session = channelService.getSession(channel);
		if (session != null) {
			Timer handleRequestTimer = metrics.timer(name(SocketServerHandler.class, "handleRequest"));
			Timer.Context context = handleRequestTimer.time();
			messageExecutor.execute(session, message);
			context.stop();
		}
		logger.debug(message.toString());
	}

	public void send(Session session, Message message) {
		Channel channel = channelService.getChannel(session.getSessionId());
		if (channel != null) {
			ChannelFuture future = channel.writeAndFlush(message);
			future.addListener(new ChannelFutureListener() {
				@Override
				public void operationComplete(ChannelFuture future) throws Exception {
					if (future.isSuccess() && logger.isDebugEnabled()) {
						logger.debug("[DEBUG] [RESPONSE]\n" + message.toString());
					}
				}
			});
		}
	}


	public void send(List<Session> receivers, final Message message) {
		for (Session receiver : receivers) {
			send(receiver, message);
		}
	}


	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		logger.error(cause);
		ctx.close();
	}


	@Override
	public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
		Channel channel = ctx.channel();
		Session session = channelService.getSession(channel);

		handleDisconnectedRequest(session);

		channelService.remove(channel);
	}

	protected abstract void handleConnectedRequest(Session session);

	protected abstract void handleDisconnectedRequest(Session session);

}
