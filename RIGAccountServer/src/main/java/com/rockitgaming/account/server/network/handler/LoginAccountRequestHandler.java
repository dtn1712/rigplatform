package com.rockitgaming.account.server.network.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rockitgaming.account.server.network.AccountErrorCode;
import com.rockitgaming.common.util.MapperUtils;
import com.rockitgaming.data.model.entity.account.Account;
import com.rockitgaming.network.domain.Message;
import com.rockitgaming.network.domain.Session;
import com.rockitgaming.network.socket.SocketCommandType;
import com.rockitgaming.network.socket.SocketErrorCode;
import com.rockitgaming.network.socket.handler.BaseValidateInputRequestHandler;
import com.rockitgaming.network.socket.message.MessageFactory;
import com.rockitgaming.service.AccountService;
import com.rockitgaming.service.exceptions.AccountAccessException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class LoginAccountRequestHandler extends BaseValidateInputRequestHandler {

    private static final Logger logger = LogManager.getLogger(RegisterAccountRequestHandler.class);

    @Autowired
    private AccountService accountService;

    public LoginAccountRequestHandler(List<String> requireFields) {
        super(requireFields);
    }

    @Override
    protected void handleRequest(Session session, Message message, Map<String, Object> messageData) {
        if (session.isAuthenticated()) {
            writeMessage(session, MessageFactory.createErrorMessage(AccountErrorCode.ERROR_ALREADY_LOGIN,
                    String.format("This session %s from IP %s already login", session.getSessionId(), session.getClientIp())));
            return;
        }

        try {
            ObjectMapper objectMapper = MapperUtils.getObjectMapper();
            String loginType = (String) messageData.get("loginType");
            String loginKey = (String) messageData.get("loginKey");
            String loginValue = (String) messageData.get("loginValue");
            Account account = accountService.login(loginType, loginKey, loginValue, session.getClientIp());
            Message successfulMessage = MessageFactory.createMessage(SocketCommandType.COMMAND_SUCCESS);
            successfulMessage.putString(Message.DATA_KEY, objectMapper.writeValueAsString(account));
            session.setAuthenticated(true);
            session.setLoginTime(account.getLastSuccessLoginTime());
            session.setLoginUsername(account.getUsername());
            writeMessage(session, successfulMessage);
        } catch (AccountAccessException e) {
            logger.error("Failed to login account", e);
            writeMessage(session, MessageFactory.createErrorMessage(AccountErrorCode.ERROR_LOGIN_ACCOUNT, e.getMessage()));
        } catch (JsonProcessingException e) {
            logger.error("Failed to write output", e);
            writeMessage(session, MessageFactory.createErrorMessage(SocketErrorCode.ERROR_HANDLE_REQUEST,"Failed to handle request. Please try again"));
        }

    }
}
