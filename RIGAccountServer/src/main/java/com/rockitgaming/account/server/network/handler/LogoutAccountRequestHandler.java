package com.rockitgaming.account.server.network.handler;

import com.rockitgaming.data.model.entity.account.Account;
import com.rockitgaming.network.domain.Message;
import com.rockitgaming.network.domain.Session;
import com.rockitgaming.network.socket.SocketErrorCode;
import com.rockitgaming.network.socket.channel.ChannelService;
import com.rockitgaming.network.socket.message.MessageFactory;
import com.rockitgaming.service.exceptions.AccountAccessException;
import io.netty.channel.Channel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Component
public class LogoutAccountRequestHandler extends BaseAuthorizeSessionRequestHandler {

    @Autowired
    private ChannelService channelService;

    private static final Logger logger = LogManager.getLogger(LogoutAccountRequestHandler.class);

    public LogoutAccountRequestHandler(List<String> requireFields) {
        super(requireFields);
    }

    @Override
    public void handleAuthorizedSessionRequest(Session session, Message message, Map<String, Object> messageData) {
        try {
            String username = (String) messageData.get("username");
            if (session.getLoginUsername() == null || !Objects.equals(username, session.getLoginUsername())) {
                throw new AccountAccessException("The request logout user and current login user does not match");
            }
            Account account = new Account();
            account.setUsername(username);
            accountService.logout(account);

            // Remove channel and also close the connection
            Channel channel = channelService.getChannel(session.getSessionId());
            channelService.remove(channel);
            channel.close();
        } catch (Exception e) {
            logger.error("Failed to handle request", e);
            writeMessage(session, MessageFactory.createErrorMessage(SocketErrorCode.ERROR_HANDLE_REQUEST, e.getMessage()));
        }
    }
}
