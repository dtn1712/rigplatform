package com.rockitgaming.account.server.network;

/**
 * Game Error Code start with 100 till 999
 */
public class AccountErrorCode {

    public static final short ERROR_ALREADY_LOGIN = 100;
    public static final short ERROR_REGISTER_ACCOUNT = 101;
    public static final short ERROR_LOGIN_ACCOUNT = 102;
}
