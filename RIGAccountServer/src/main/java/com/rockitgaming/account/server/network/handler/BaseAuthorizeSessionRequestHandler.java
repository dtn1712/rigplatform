package com.rockitgaming.account.server.network.handler;

import com.rockitgaming.network.domain.Message;
import com.rockitgaming.network.domain.Session;
import com.rockitgaming.network.socket.SocketErrorCode;
import com.rockitgaming.network.socket.handler.BaseValidateInputRequestHandler;
import com.rockitgaming.network.socket.message.MessageFactory;
import com.rockitgaming.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

public abstract class BaseAuthorizeSessionRequestHandler extends BaseValidateInputRequestHandler {

    @Autowired
    protected AccountService accountService;

    public BaseAuthorizeSessionRequestHandler(List<String> requireFields) {
        super(requireFields);
    }

    @Override
    public void handleRequest(Session session, Message message, Map<String, Object> messageData) {
        if (!session.isAuthenticated()) {
            String username = (String) messageData.get("username");
            if (username == null || !accountService.isCurrentLogin(username, session.getClientIp())) {
                writeMessage(session, MessageFactory.createErrorMessage(SocketErrorCode.ERROR_UNAUTHORIZED_SESSION,
                        String.format("This session %s from IP %s is not authorized", session.getSessionId(), session.getClientIp())));
                return;
            } else {
                session.setAuthenticated(true);
                session.setLoginUsername(username);
            }
        }

        handleAuthorizedSessionRequest(session, message, messageData);
    }

    public abstract void handleAuthorizedSessionRequest(Session session, Message message, Map<String, Object> messageData);
}
