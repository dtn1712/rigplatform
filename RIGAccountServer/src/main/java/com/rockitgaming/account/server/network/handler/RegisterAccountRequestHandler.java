package com.rockitgaming.account.server.network.handler;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rockitgaming.account.server.network.AccountErrorCode;
import com.rockitgaming.common.util.MapperUtils;
import com.rockitgaming.data.model.entity.account.Account;
import com.rockitgaming.network.domain.Message;
import com.rockitgaming.network.domain.Session;
import com.rockitgaming.network.socket.SocketCommandType;
import com.rockitgaming.network.socket.SocketErrorCode;
import com.rockitgaming.network.socket.handler.BaseValidateInputRequestHandler;
import com.rockitgaming.network.socket.message.MessageFactory;
import com.rockitgaming.service.AccountService;
import com.rockitgaming.service.exceptions.AccountRegisterException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class RegisterAccountRequestHandler extends BaseValidateInputRequestHandler {

    private static final Logger logger = LogManager.getLogger(RegisterAccountRequestHandler.class);

    @Autowired
    private AccountService accountService;

    public RegisterAccountRequestHandler(List<String> requireFields) {
        super(requireFields);
    }

    @Override
    public void handleRequest(Session session, Message message, Map<String, Object> messageData) {
        ObjectMapper objectMapper = MapperUtils.getObjectMapper();
        try {
            String username = (String) messageData.get("username");
            String email = (String) messageData.get("email");
            String phone = (String) messageData.get("phone");
            String password = (String) messageData.get("password");

            if (email == null && phone == null) {
                writeMessage(session, MessageFactory.createErrorMessage(SocketErrorCode.ERROR_INVALID_INPUT, "Input need to have either email or phone"));
                return;
            }

            Account account = new Account();
            account.setUsername(username);
            account.setEmail(email);
            account.setPhone(phone);
            account.setPassword(password);
            account.setLastSuccessLoginTime(new DateTime());

            Account createdAccount = accountService.register(account, session.getClientIp());

            session.setAuthenticated(true);
            session.setLoginTime(createdAccount.getLastSuccessLoginTime());
            session.setLoginUsername(createdAccount.getUsername());
            Message successfulMessage = MessageFactory.createMessage(SocketCommandType.COMMAND_SUCCESS);
            successfulMessage.putString(Message.DATA_KEY, objectMapper.writeValueAsString(createdAccount));
            writeMessage(session, successfulMessage);
        } catch (AccountRegisterException e) {
            logger.error("Failed to register account", e);
            writeMessage(session, MessageFactory.createErrorMessage(AccountErrorCode.ERROR_REGISTER_ACCOUNT, e.getMessage()));
        } catch (JsonProcessingException e) {
            logger.error("Failed to write output", e);
            writeMessage(session, MessageFactory.createErrorMessage(SocketErrorCode.ERROR_HANDLE_REQUEST,"Failed to handle request. Please try again"));
        }
    }


}
