package com.rockitgaming.account.server;

/**
 * Created by dangn on 7/10/16.
 */
public class Constants {

    public static final String APPLICATION_NAME = "RIGAccountServer";

    public static final String CONFIG_FOLDER_PATH = APPLICATION_NAME + "/src/main/config/";

    public static final String APPLICATION_CONTEXT_PATH = "spring/beanconfig/application-context.xml";


}
