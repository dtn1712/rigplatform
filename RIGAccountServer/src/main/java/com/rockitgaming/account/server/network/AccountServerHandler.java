package com.rockitgaming.account.server.network;

import com.rockitgaming.data.model.entity.account.Account;
import com.rockitgaming.network.domain.Session;
import com.rockitgaming.network.socket.SocketServerHandler;
import com.rockitgaming.service.AccountService;
import io.netty.channel.ChannelHandler.Sharable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Sharable
public class AccountServerHandler extends SocketServerHandler{

    private static final Logger logger = LogManager.getLogger(AccountServerHandler.class);

    @Autowired
    private AccountService accountService;

    @Override
    protected void handleConnectedRequest(Session session) {
        /** TODO: Add metric **/
    }

    @Override
    protected void handleDisconnectedRequest(Session session) {
        if (session != null && session.isAuthenticated()) {
            String loginUsername = session.getLoginUsername();
            if (loginUsername != null) {
                try {
                    Account account = new Account();
                    account.setUsername(loginUsername);
                    accountService.logout(account);
                } catch (Exception e) {
                    logger.error(String.format("Failed to log out user %s", loginUsername), e);
                }
            }
        }
        /** TODO: Add metric **/
    }
}
