package com.rockitgaming.account.server;

import com.rockitgaming.network.ServerConfig;
import com.rockitgaming.network.socket.SocketServer;
import com.rockitgaming.network.util.FileUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AccountServer {

    public static void initConfig() {
        ServerConfig.init(FileUtils.loadConfigFile(Constants.CONFIG_FOLDER_PATH + "server.properties"));
    }


    public static void main(String[] args) throws Exception {
        initConfig();

        ApplicationContext context = new ClassPathXmlApplicationContext(Constants.APPLICATION_CONTEXT_PATH);
        SocketServer server = context.getBean(SocketServer.class);
        server.start();
    }
}
