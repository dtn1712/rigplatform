# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.16)
# Database: pokemon_test
# Generation Time: 2016-11-16 10:34:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table ability
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ability`;

CREATE TABLE `ability` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `effectDescription` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


# Dump of table account
# ------------------------------------------------------------

DROP TABLE IF EXISTS `game`;

CREATE TABLE `game` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `description` varchar(254),
  `gameType` varchar(30) NOT NULL,
  `gameStatus` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


# Dump of table account
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(254),
  `phone` varchar(30),
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastSuccessLoginTime` datetime,
  `accountType` varchar(30) NOT NULL,
  `accountStatus` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table accountattemptlogin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accountattemptlogin`;

CREATE TABLE `accountattemptlogin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `isLoginSuccess` tinyint(1) NOT NULL,
  `isSessionLogout` tinyint(1) NOT NULL DEFAULT 0,
  `loginTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `loginIP` varchar(255) DEFAULT NULL,
  `accountId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `accountattemptlogin_accountId_671ec8e6_fk_accountId` (`accountId`),
  CONSTRAINT `accountattemptlogin_accountId_671ec8e6_fk_accountId` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table accountlock
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accountlock`;

CREATE TABLE `accountlock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lockIP` text NOT NULL,
  `reason` text NOT NULL,
  `duration` int(11) NOT NULL,
  `lockStartTime` datetime(6) NOT NULL,
  `lockEndTime` datetime(6) NOT NULL,
  `accountId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `accountlock_accountId_5e7b7f39_fk_accountId` (`accountId`),
  CONSTRAINT `accountlock_accountId_5e7b7f39_fk_accountId` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table badge
# ------------------------------------------------------------

DROP TABLE IF EXISTS `badge`;

CREATE TABLE `badge` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `increasedStats` varchar(255) NOT NULL,
  `badgeType` varchar(100) NOT NULL,
  `sprite` int(11) NOT NULL,
  `obeyingPokemonLevel` int(11) DEFAULT NULL,
  `locationId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `badge_e274a5da` (`locationId`),
  CONSTRAINT `badge_locationId_940fd90b_fk_locationId` FOREIGN KEY (`locationId`) REFERENCES `location` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table badge_givers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `badge_givers`;

CREATE TABLE `badge_givers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `badgeId` bigint(20) NOT NULL,
  `playerId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `badge_givers_badgeId_82ec64e3_uniq` (`badgeId`,`playerId`),
  KEY `badge_givers_playerId_cbabd10b_fk_playerId` (`playerId`),
  CONSTRAINT `badge_givers_badgeId_9a5ae9f9_fk_badgeId` FOREIGN KEY (`badgeId`) REFERENCES `badge` (`id`),
  CONSTRAINT `badge_givers_playerId_cbabd10b_fk_playerId` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table friend
# ------------------------------------------------------------

DROP TABLE IF EXISTS `friend`;

CREATE TABLE `friend` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `friendTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `player1Id` bigint(20) NOT NULL,
  `player2Id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `friend_26d85d6c` (`player1Id`),
  KEY `friend_863818d8` (`player2Id`),
  CONSTRAINT `friend_player1Id_3b46ec6a_fk_playerId` FOREIGN KEY (`player1Id`) REFERENCES `player` (`id`),
  CONSTRAINT `friend_player2Id_eaf2f26f_fk_playerId` FOREIGN KEY (`player2Id`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table friendrequest
# ------------------------------------------------------------

DROP TABLE IF EXISTS `friendrequest`;

CREATE TABLE `friendrequest` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `requestStatus` varchar(30) NOT NULL,
  `sendTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fromPlayerId` bigint(20) NOT NULL,
  `toPlayerId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `friendrequest_431a9b20` (`fromPlayerId`),
  KEY `friendrequest_4375866c` (`toPlayerId`),
  CONSTRAINT `friendrequest_fromPlayerId_531ffcbf_fk_playerId` FOREIGN KEY (`fromPlayerId`) REFERENCES `player` (`id`),
  CONSTRAINT `friendrequest_toPlayerId_f4cfb7ee_fk_playerId` FOREIGN KEY (`toPlayerId`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table growthrate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `growthrate`;

CREATE TABLE `growthrate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `formula` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `item`;

CREATE TABLE `item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `cost` int(11) NOT NULL,
  `effectDescription` text NOT NULL,
  `categoryId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_b583a629` (`categoryId`),
  CONSTRAINT `item_categoryId_dfd2ff4c_fk_itemcategoryId` FOREIGN KEY (`categoryId`) REFERENCES `itemcategory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table itemcategory
# ------------------------------------------------------------

DROP TABLE IF EXISTS `itemcategory`;

CREATE TABLE `itemcategory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `key` varchar(50) NOT NULL,
  `pocketId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `itemcategory_abb874df` (`pocketId`),
  CONSTRAINT `itemcategory_pocketId_703a7909_fk_itempocketId` FOREIGN KEY (`pocketId`) REFERENCES `itempocket` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table itempocket
# ------------------------------------------------------------

DROP TABLE IF EXISTS `itempocket`;

CREATE TABLE `itempocket` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `key` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table levelexperience
# ------------------------------------------------------------

DROP TABLE IF EXISTS `levelexperience`;

CREATE TABLE `levelexperience` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `level` int(11) NOT NULL,
  `experience` int(11) NOT NULL,
  `growthRateId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `levelexperience_growthRateId_037a6586_fk_growthrateId` (`growthRateId`),
  CONSTRAINT `levelexperience_growthRateId_037a6586_fk_growthrateId` FOREIGN KEY (`growthRateId`) REFERENCES `growthrate` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table location
# ------------------------------------------------------------

DROP TABLE IF EXISTS `location`;

CREATE TABLE `location` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `regionId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `location_0f442f96` (`regionId`),
  CONSTRAINT `location_regionId_0e9bbf12_fk_regionId` FOREIGN KEY (`regionId`) REFERENCES `region` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table nature
# ------------------------------------------------------------

DROP TABLE IF EXISTS `nature`;

CREATE TABLE `nature` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `increasedStat` varchar(30) NOT NULL,
  `decreasedStat` varchar(30) NOT NULL,
  `hatedFlavor` varchar(30) NOT NULL,
  `likedFlavor` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table player
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player`;

CREATE TABLE `player` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `speed` int(11) NOT NULL,
  `money` int(11) NOT NULL,
  `sprite` int(11) DEFAULT NULL,
  `posX` int(11) DEFAULT NULL,
  `posY` int(11) DEFAULT NULL,
  `mapX` int(11) DEFAULT NULL,
  `mapY` int(11) DEFAULT NULL,
  `gender` int(11) NOT NULL,
  `skin` int(11) NOT NULL,
  `hairStyle` int(11) NOT NULL,
  `hairColor` int(11) NOT NULL,
  `playerStatus` varchar(30) NOT NULL,
  `playerType` varchar(30) NOT NULL,
  `language` varchar(30) NOT NULL,
  `isMuted` tinyint(1) NOT NULL,
  `clothingId` bigint(20),
  `hatId` bigint(20),
  `vehicleId` bigint(20),
  `accountId` bigint(20) NOT NULL,
  `lastPlayTime` datetime,
  PRIMARY KEY (`id`),
  KEY `player_account_id_86300558_fk_accountId` (`accountId`),
  KEY `player_clothing_id_2508a1c6_fk_itemId` (`clothingId`),
  KEY `player_hat_id_835fd8c5_fk_itemId` (`hatId`),
  KEY `player_35ec04dc` (`vehicleId`),
  CONSTRAINT `player_accountId_86300558_fk_accountId` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`),
  CONSTRAINT `player_clothingId_2508a1c6_fk_itemId` FOREIGN KEY (`clothingId`) REFERENCES `item` (`id`),
  CONSTRAINT `player_hatId_835fd8c5_fk_itemId` FOREIGN KEY (`hatId`) REFERENCES `item` (`id`),
  CONSTRAINT `player_vehicleId_7d48ab76_fk_vehicleId` FOREIGN KEY (`vehicleId`) REFERENCES `vehicle` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table player_badges
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player_badges`;

CREATE TABLE `player_badges` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `playerId` bigint(20) NOT NULL,
  `badgeId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `player_badges_playerId_40fbeb2c_uniq` (`playerId`,`badgeId`),
  KEY `player_badges_badgeId_f72f2270_fk_badgeId` (`badgeId`),
  CONSTRAINT `player_badges_badgeId_f72f2270_fk_badgeId` FOREIGN KEY (`badgeId`) REFERENCES `badge` (`id`),
  CONSTRAINT `player_badges_playerId_3a961540_fk_playerId` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table player_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `player_items`;

CREATE TABLE `player_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `playerId` bigint(20) NOT NULL,
  `itemId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `player_items_playerId_38ebeb31_uniq` (`playerId`,`itemId`),
  KEY `player_items_itemId_59d9f5f5_fk_itemId` (`itemId`),
  CONSTRAINT `player_items_itemId_59d9f5f5_fk_itemId` FOREIGN KEY (`itemId`) REFERENCES `item` (`id`),
  CONSTRAINT `player_items_playerId_62cdad8f_fk_playerId` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table playerbagitem
# ------------------------------------------------------------

DROP TABLE IF EXISTS `playerbagitem`;

CREATE TABLE `playerbagitem` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `itemId` bigint(20) NOT NULL,
  `playerId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `playerbagitem_itemId_a929791e_fk_itemId` (`itemId`),
  KEY `playerbagitem_playerId_a39b83ec_fk_playerId` (`playerId`),
  CONSTRAINT `playerbagitem_itemId_a929791e_fk_itemId` FOREIGN KEY (`itemId`) REFERENCES `item` (`id`),
  CONSTRAINT `playerbagitem_playerId_a39b83ec_fk_playerId` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table playerparty
# ------------------------------------------------------------

DROP TABLE IF EXISTS `playerparty`;

CREATE TABLE `playerparty` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `playerId` bigint(20) NOT NULL,
  `pokemon1Id` bigint(20) NOT NULL,
  `pokemon2Id` bigint(20) NOT NULL,
  `pokemon3Id` bigint(20) NOT NULL,
  `pokemon4Id` bigint(20) NOT NULL,
  `pokemon5Id` bigint(20) NOT NULL,
  `pokemon6Id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `playerparty_playerId_c551e63d_fk_playerId` (`playerId`),
  KEY `playerparty_bdb3ea80` (`pokemon1Id`),
  KEY `playerparty_b7605625` (`pokemon2Id`),
  KEY `playerparty_da06f32b` (`pokemon3Id`),
  KEY `playerparty_1562b61a` (`pokemon4Id`),
  KEY `playerparty_57ae1354` (`pokemon5Id`),
  KEY `playerparty_0f932eef` (`pokemon6Id`),
  CONSTRAINT `playerparty_playerId_c551e63d_fk_playerId` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`),
  CONSTRAINT `playerparty_pokemon1Id_a9ccd660_fk_pokemonId` FOREIGN KEY (`pokemon1Id`) REFERENCES `pokemon` (`id`),
  CONSTRAINT `playerparty_pokemon2Id_619ba197_fk_pokemonId` FOREIGN KEY (`pokemon2Id`) REFERENCES `pokemon` (`id`),
  CONSTRAINT `playerparty_pokemon3Id_245b0131_fk_pokemonId` FOREIGN KEY (`pokemon3Id`) REFERENCES `pokemon` (`id`),
  CONSTRAINT `playerparty_pokemon4Id_77d6f2c9_fk_pokemonId` FOREIGN KEY (`pokemon4Id`) REFERENCES `pokemon` (`id`),
  CONSTRAINT `playerparty_pokemon5Id_2e1f40ca_fk_pokemonId` FOREIGN KEY (`pokemon5Id`) REFERENCES `pokemon` (`id`),
  CONSTRAINT `playerparty_pokemon6Id_9ed2f6d0_fk_pokemonId` FOREIGN KEY (`pokemon6Id`) REFERENCES `pokemon` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table playerpokedex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `playerpokedex`;

CREATE TABLE `playerpokedex` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `isOwned` tinyint(1) NOT NULL,
  `playerId` bigint(20) NOT NULL,
  `pokemonSpeciesId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `playerpokedex_playerId_95fa6a49_fk_playerId` (`playerId`),
  KEY `playerpokedex_e44a102e` (`pokemonSpeciesId`),
  CONSTRAINT `playerpo_pokemonSpeciesId_50b7d696_fk_pokemonspeciesId` FOREIGN KEY (`pokemonSpeciesId`) REFERENCES `pokemonspecies` (`id`),
  CONSTRAINT `playerpokedex_playerId_95fa6a49_fk_playerId` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table playerstat
# ------------------------------------------------------------

DROP TABLE IF EXISTS `playerstat`;

CREATE TABLE `playerstat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `numWins` int(11) NOT NULL,
  `numLosses` int(11) NOT NULL,
  `playerId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `playerstat_playerId_b27c4ac9_fk_playerId` (`playerId`),
  CONSTRAINT `playerstat_playerId_b27c4ac9_fk_playerId` FOREIGN KEY (`playerId`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table pokemon
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pokemon`;

CREATE TABLE `pokemon` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `level` int(11) NOT NULL,
  `currentExp` int(11) NOT NULL,
  `baseExp` int(11) NOT NULL,
  `isFainted` tinyint(1) NOT NULL,
  `currentAccuracy` int(11) NOT NULL,
  `currentEvasion` int(11) NOT NULL,
  `hp` int(11) NOT NULL,
  `currentHP` int(11) NOT NULL,
  `attack` int(11) NOT NULL,
  `defend` int(11) NOT NULL,
  `speed` int(11) NOT NULL,
  `specialAttack` int(11) NOT NULL,
  `specialDefend` int(11) NOT NULL,
  `evHP` int(11) NOT NULL,
  `evAttack` int(11) NOT NULL,
  `evDefend` int(11) NOT NULL,
  `evSpeed` int(11) NOT NULL,
  `evSpecialAttack` int(11) NOT NULL,
  `evSpecialDefend` int(11) NOT NULL,
  `ivHP` int(11) NOT NULL,
  `ivAttack` int(11) NOT NULL,
  `ivDefend` int(11) NOT NULL,
  `ivSpeed` int(11) NOT NULL,
  `ivSpecialAttack` int(11) NOT NULL,
  `ivSpecialDefend` int(11) NOT NULL,
  `caughtTime` datetime DEFAULT NULL,
  `currentTrainerId` bigint(20) DEFAULT NULL,
  `move1Id` bigint(20),
  `move2Id` bigint(20),
  `move3Id` bigint(20),
  `move4Id` bigint(20),
  `natureId` bigint(20) NOT NULL,
  `originalTrainerId` bigint(20) NOT NULL,
  `pokemonSpeciesId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pokemon_currentTrainerId_bdf8e1f4_fk_playerId` (`currentTrainerId`),
  KEY `pokemon_16b0f155` (`move1Id`),
  KEY `pokemon_eca25928` (`move2Id`),
  KEY `pokemon_6863995d` (`move3Id`),
  KEY `pokemon_5e44600d` (`move4Id`),
  KEY `pokemon_5c0ad424` (`natureId`),
  KEY `pokemon_067bcdc9` (`originalTrainerId`),
  KEY `pokemon_e44a102e` (`pokemonSpeciesId`),
  CONSTRAINT `pokemon_currentTrainerId_bdf8e1f4_fk_playerId` FOREIGN KEY (`currentTrainerId`) REFERENCES `player` (`id`),
  CONSTRAINT `pokemon_move1Id_686bdb64_fk_pokemonmoveId` FOREIGN KEY (`move1Id`) REFERENCES `pokemonmove` (`id`),
  CONSTRAINT `pokemon_move2Id_2e952fa3_fk_pokemonmoveId` FOREIGN KEY (`move2Id`) REFERENCES `pokemonmove` (`id`),
  CONSTRAINT `pokemon_move3Id_c7a4dba3_fk_pokemonmoveId` FOREIGN KEY (`move3Id`) REFERENCES `pokemonmove` (`id`),
  CONSTRAINT `pokemon_move4Id_3dca4c22_fk_pokemonmoveId` FOREIGN KEY (`move4Id`) REFERENCES `pokemonmove` (`id`),
  CONSTRAINT `pokemon_natureId_ae506f17_fk_natureId` FOREIGN KEY (`natureId`) REFERENCES `nature` (`id`),
  CONSTRAINT `pokemon_originalTrainerId_2317e746_fk_playerId` FOREIGN KEY (`originalTrainerId`) REFERENCES `player` (`id`),
  CONSTRAINT `pokemon_pokemonSpeciesId_e0c0aa8c_fk_pokemonspeciesId` FOREIGN KEY (`pokemonSpeciesId`) REFERENCES `pokemonspecies` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table pokemonability
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pokemonability`;

CREATE TABLE `pokemonability` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `isHidden` tinyint(1) NOT NULL,
  `abilityId` bigint(20) NOT NULL,
  `pokemonId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pokemonability_abilityId_1e128e49_fk_abilityId` (`abilityId`),
  KEY `pokemonability_pokemonId_56a8fb6d_fk_pokemonId` (`pokemonId`),
  CONSTRAINT `pokemonability_abilityId_1e128e49_fk_abilityId` FOREIGN KEY (`abilityId`) REFERENCES `ability` (`id`),
  CONSTRAINT `pokemonability_pokemonId_56a8fb6d_fk_pokemonId` FOREIGN KEY (`pokemonId`) REFERENCES `pokemon` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table pokemonmove
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pokemonmove`;

CREATE TABLE `pokemonmove` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `effectDescription` text NOT NULL,
  `power` int(11) DEFAULT NULL,
  `powerPoint` int(11) DEFAULT NULL,
  `accuracy` int(11) DEFAULT NULL,
  `target` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `damageClass` varchar(30) NOT NULL,
  `moveTypeId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pokemonmove_9734431c` (`moveTypeId`),
  CONSTRAINT `pokemonmove_moveTypeId_c537803b_fk_pokemontypeId` FOREIGN KEY (`moveTypeId`) REFERENCES `pokemontype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table pokemonspecies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pokemonspecies`;

CREATE TABLE `pokemonspecies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `height` double NOT NULL,
  `weight` double NOT NULL,
  `color` varchar(30) NOT NULL,
  `shape` varchar(30) NOT NULL,
  `habitat` varchar(30) NOT NULL,
  `genderRate` int(11) NOT NULL,
  `captureRate` int(11) NOT NULL,
  `baseHappiness` int(11) NOT NULL,
  `isBaby` tinyint(1) NOT NULL,
  `isGenderDifference` tinyint(1) NOT NULL,
  `hatchCounter` int(11) NOT NULL,
  `evolvesFromSpeciesId` bigint(20) DEFAULT NULL,
  `growthRateId` bigint(20) NOT NULL,
  `pokemonType1Id` bigint(20) NOT NULL,
  `pokemonType2Id` bigint(20),
  PRIMARY KEY (`id`),
  KEY `poke_evolvesFromSpeciesId_d8cbe057_fk_pokemonspeciesId` (`evolvesFromSpeciesId`),
  KEY `pokemonspecies_growthRateId_884ebe22_fk_growthrateId` (`growthRateId`),
  KEY `pokemonspecies_c5d3feee` (`pokemonType1Id`),
  KEY `pokemonspecies_aa7e9364` (`pokemonType2Id`),
  CONSTRAINT `poke_evolvesFromSpeciesId_d8cbe057_fk_pokemonspeciesId` FOREIGN KEY (`evolvesFromSpeciesId`) REFERENCES `pokemonspecies` (`id`),
  CONSTRAINT `pokemonspecie_pokemonType1Id_b5e28ada_fk_pokemontypeId` FOREIGN KEY (`pokemonType1Id`) REFERENCES `pokemontype` (`id`),
  CONSTRAINT `pokemonspecie_pokemonType2Id_57919660_fk_pokemontypeId` FOREIGN KEY (`pokemonType2Id`) REFERENCES `pokemontype` (`id`),
  CONSTRAINT `pokemonspecies_growthRateId_884ebe22_fk_growthrateId` FOREIGN KEY (`growthRateId`) REFERENCES `growthrate` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table pokemontype
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pokemontype`;

CREATE TABLE `pokemontype` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `damageClass` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table pokemontypeeffect
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pokemontypeeffect`;

CREATE TABLE `pokemontypeeffect` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `effectType` varchar(30) NOT NULL,
  `effectRate` double NOT NULL,
  `fromPokemonTypeId` bigint(20) NOT NULL,
  `toPokemonTypeId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pokemontyp_fromPokemonTypeId_b1e567a9_fk_pokemontypeId` (`fromPokemonTypeId`),
  KEY `pokemontypee_toPokemonTypeId_91c0df2c_fk_pokemontypeId` (`toPokemonTypeId`),
  CONSTRAINT `pokemontyp_fromPokemonTypeId_b1e567a9_fk_pokemontypeId` FOREIGN KEY (`fromPokemonTypeId`) REFERENCES `pokemontype` (`id`),
  CONSTRAINT `pokemontypee_toPokemonTypeId_91c0df2c_fk_pokemontypeId` FOREIGN KEY (`toPokemonTypeId`) REFERENCES `pokemontype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table region
# ------------------------------------------------------------

DROP TABLE IF EXISTS `region`;

CREATE TABLE `region` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table trade
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trade`;

CREATE TABLE `trade` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` varchar(30) NOT NULL,
  `tradingTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trader1Id` bigint(20) NOT NULL,
  `trader2Id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trade_trader1Id_ca2d3c18_fk_playerId` (`trader1Id`),
  KEY `trade_trader2Id_367adf19_fk_playerId` (`trader2Id`),
  CONSTRAINT `trade_trader1Id_ca2d3c18_fk_playerId` FOREIGN KEY (`trader1Id`) REFERENCES `player` (`id`),
  CONSTRAINT `trade_trader2Id_367adf19_fk_playerId` FOREIGN KEY (`trader2Id`) REFERENCES `player` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tradeitem
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tradeitem`;

CREATE TABLE `tradeitem` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `itemsTrader1Id` bigint(20) NOT NULL,
  `itemsTrader2Id` bigint(20) NOT NULL,
  `tradeId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tradeitem_itemsTrader1Id_961761af_fk_itemId` (`itemsTrader1Id`),
  KEY `tradeitem_itemsTrader2Id_ea98dde8_fk_itemId` (`itemsTrader2Id`),
  KEY `tradeitem_tradeId_a4e34e95_fk_tradeId` (`tradeId`),
  CONSTRAINT `tradeitem_itemsTrader1Id_961761af_fk_itemId` FOREIGN KEY (`itemsTrader1Id`) REFERENCES `item` (`id`),
  CONSTRAINT `tradeitem_itemsTrader2Id_ea98dde8_fk_itemId` FOREIGN KEY (`itemsTrader2Id`) REFERENCES `item` (`id`),
  CONSTRAINT `tradeitem_tradeId_a4e34e95_fk_tradeId` FOREIGN KEY (`tradeId`) REFERENCES `trade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tradepokemon
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tradepokemon`;

CREATE TABLE `tradepokemon` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pokemonTrader1Id` bigint(20) NOT NULL,
  `pokemonTrader2Id` bigint(20) NOT NULL,
  `tradeId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tradepokemon_pokemonTrader1Id_5dbe79f9_fk_pokemonId` (`pokemonTrader1Id`),
  KEY `tradepokemon_pokemonTrader2Id_f5955cd8_fk_pokemonId` (`pokemonTrader2Id`),
  KEY `tradepokemon_tradeId_0f2b5f20_fk_tradeId` (`tradeId`),
  CONSTRAINT `tradepokemon_pokemonTrader1Id_5dbe79f9_fk_pokemonId` FOREIGN KEY (`pokemonTrader1Id`) REFERENCES `pokemon` (`id`),
  CONSTRAINT `tradepokemon_pokemonTrader2Id_f5955cd8_fk_pokemonId` FOREIGN KEY (`pokemonTrader2Id`) REFERENCES `pokemon` (`id`),
  CONSTRAINT `tradepokemon_tradeId_0f2b5f20_fk_tradeId` FOREIGN KEY (`tradeId`) REFERENCES `trade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table vehicle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `vehicle`;

CREATE TABLE `vehicle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `speed` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
