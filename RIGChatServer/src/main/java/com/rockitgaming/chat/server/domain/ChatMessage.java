package com.rockitgaming.chat.server.domain;

import com.rockitgaming.chat.server.converter.JodaDateTimeConverter;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.mongodb.morphia.annotations.*;

import java.util.List;

@Entity("chatmessage")
@Getter
@Setter
@Converters(JodaDateTimeConverter.class)
@Indexes({
        @Index(value = "fromPlayer", fields = @Field("fromPlayer")),
        @Index(value = "toChannel", fields = @Field("toChannel")),
        @Index(value = "sendTime", fields = @Field("sendTime")),
        @Index(value = "messageStatus", fields = @Field("messageStatus"))
})
public class ChatMessage {

    @Id
    private ObjectId id;

    @Property
    private String fromAccount;

    @Property
    private String fromPlayerName;

    @Property
    private String toChannel;

    @Property
    private String messageContent;

    @Property
    private DateTime sendTime;

    @Property
    private DateTime lastUpdateTime;

    @Property
    private ChatMessageStatus messageStatus;

    @Property
    private List<String> forbiddenWords;

    @Property
    private boolean isBadMessage;
}
