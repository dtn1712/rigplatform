package com.rockitgaming.chat.server.domain;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 1/7/17
 * Time: 1:54 AM
 */
public enum ChatMessageStatus {

    SENT, RECEIVED, READ
}
