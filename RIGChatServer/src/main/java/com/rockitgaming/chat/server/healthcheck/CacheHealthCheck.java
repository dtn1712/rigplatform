package com.rockitgaming.chat.server.healthcheck;

import com.codahale.metrics.health.HealthCheck;
import com.rockitgaming.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CacheHealthCheck extends HealthCheck {

    private final CacheService cacheService;

    @Autowired
    public CacheHealthCheck(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @Override
    protected Result check() throws Exception {
        if (cacheService.ping()) {
            return Result.healthy();
        }
        return Result.unhealthy("Cannot connect to cache service");
    }
}
