package com.rockitgaming.chat.server.network.handler;

import com.rockitgaming.data.model.entity.account.Account;
import com.rockitgaming.network.domain.Message;
import com.rockitgaming.network.domain.Session;
import com.rockitgaming.network.socket.SocketErrorCode;
import com.rockitgaming.network.socket.handler.BaseValidateInputRequestHandler;
import com.rockitgaming.network.socket.message.MessageFactory;
import com.rockitgaming.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

public abstract class BaseAuthorizeSessionRequestHandler extends BaseValidateInputRequestHandler {

    @Autowired
    protected AccountService accountService;

    public BaseAuthorizeSessionRequestHandler(List<String> requireFields) {
        super(requireFields);
    }

    @Override
    public void handleRequest(Session session, Message message, Map<String, Object> messageData) {
        if (!session.isAuthenticated() || session.getUser() == null) {
            String username = (String) messageData.get("accountUsername");
            boolean isAuthorized = false;
            Account loginAccount;
            if (username != null) {
                loginAccount = accountService.getCurrentLoginAccount(username, session.getClientIp());
                if (loginAccount != null) {
                    isAuthorized = true;
                }
            }
            if (!isAuthorized) {
                writeMessage(session, MessageFactory.createErrorMessage(SocketErrorCode.ERROR_UNAUTHORIZED_SESSION,
                        String.format("This session %s from IP %s is not authorized", session.getSessionId(), session.getClientIp())));
                return;
            } else {
                String playerName = (String) messageData.get("playerName");
                session.setAuthenticated(true);
                session.setUser(playerName);
                session.setLoginUsername(username);
            }
        }

        handleAuthorizedSessionRequest(session, message, messageData);
    }

    public abstract void handleAuthorizedSessionRequest(Session session, Message message, Map<String, Object> messageData);

}
