package com.rockitgaming.chat.server.healthcheck;

import com.codahale.metrics.health.HealthCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class AccountDatabaseHealthCheck extends HealthCheck {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public AccountDatabaseHealthCheck(@Qualifier("dataSource") DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    protected Result check() throws Exception {
        if (jdbcTemplate.queryForObject("SELECT 1", Long.class) == 1) {
            return Result.healthy();
        }
        return Result.unhealthy("Can't ping database");
    }
}
