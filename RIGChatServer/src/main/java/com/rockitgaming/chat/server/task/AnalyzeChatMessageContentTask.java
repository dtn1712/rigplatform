package com.rockitgaming.chat.server.task;


import com.rockitgaming.chat.server.domain.ChatMessage;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

public class AnalyzeChatMessageContentTask implements Callable<ChatMessage> {

    private List<String> forbiddenWords;
    private ChatMessage chatMessage;

    public AnalyzeChatMessageContentTask(List<String> forbiddenWords, ChatMessage chatMessage) {
        this.forbiddenWords = forbiddenWords;
        this.chatMessage = chatMessage;
    }

    @Override
    public ChatMessage call() throws Exception {
        String content = chatMessage.getMessageContent();
        List<String> containForbiddenWords = forbiddenWords.stream().filter(forbiddenWord -> content.indexOf(forbiddenWord) > -1).collect(Collectors.toList());
        if (!containForbiddenWords.isEmpty()) {
            chatMessage.setBadMessage(true);
            chatMessage.setForbiddenWords(containForbiddenWords);
        }
        return chatMessage;
    }
}
