package com.rockitgaming.chat.server.healthcheck;


import com.codahale.metrics.health.HealthCheck;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class ChatDatabaseHealthCheck extends HealthCheck {

    private String noSqlHost;
    private int noSqlPort;

    @Autowired
    protected ChatDatabaseHealthCheck(@Qualifier("noSqlHost") String noSqlHost,
                                      @Qualifier("noSqlPort") int noSqlPort) {
        this.noSqlHost = noSqlHost;
        this.noSqlPort = noSqlPort;
    }

    @Override
    protected Result check() throws Exception {
        try {
            MongoClient mongoClient = new MongoClient(noSqlHost, noSqlPort);
            mongoClient.close();
            return Result.healthy();
        } catch (Exception e) {
            return Result.unhealthy("Cannot connect to mongo db");
        }
    }
}
