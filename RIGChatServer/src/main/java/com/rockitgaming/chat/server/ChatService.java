package com.rockitgaming.chat.server;

import com.rockitgaming.chat.server.domain.ChatMessage;
import com.rockitgaming.chat.server.domain.ChatMessageRepository;
import com.rockitgaming.chat.server.task.AnalyzeChatMessageContentTask;
import com.rockitgaming.chat.server.task.PersistChatMessageDataTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.Future;

@Service
public class ChatService {

    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    @Autowired
    private ChatMessageRepository chatMessageRepository;

    @Resource(name = "forbiddenWords")
    private List<String> forbiddenWords;

    public void saveChatData(ChatMessage chatMessage) {
        PersistChatMessageDataTask persistChatMessageDataTask = new PersistChatMessageDataTask(chatMessageRepository,chatMessage);
        taskExecutor.execute(persistChatMessageDataTask);
    }

    @Async
    public Future<ChatMessage> analyzeChatMessage(ChatMessage chatMessage) {
        AnalyzeChatMessageContentTask analyzeChatMessageContentTask = new AnalyzeChatMessageContentTask(forbiddenWords, chatMessage);
        return taskExecutor.submit(analyzeChatMessageContentTask);
    }
}
