package com.rockitgaming.chat.server.task;

import com.mongodb.DuplicateKeyException;
import com.rockitgaming.chat.server.domain.ChatMessage;
import com.rockitgaming.chat.server.domain.ChatMessageRepository;

public class PersistChatMessageDataTask implements Runnable {

    private ChatMessage chatMessage;
    private ChatMessageRepository chatMessageRepository;

    public PersistChatMessageDataTask(ChatMessageRepository chatMessageRepository, ChatMessage chatMessage) {
        this.chatMessageRepository = chatMessageRepository;
        this.chatMessage = chatMessage;
    }

    @Override
    public void run() {
        try {
            chatMessageRepository.save(chatMessage);
        } catch (DuplicateKeyException e) {
            chatMessageRepository.update(chatMessage);
        }
    }
}
